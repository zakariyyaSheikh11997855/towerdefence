﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GlobalAmmo : MonoBehaviour {
	public static int currentAmmo;
	int internalAmmo;
	public GameObject ammoDisplay;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		internalAmmo = currentAmmo;
		ammoDisplay.GetComponent<Text> ().text = "" + internalAmmo;
	}
}
